# heroku-buildpack-deploy-key
Heroku buildpack to let you use deploy key to access your private repositories. This enables
bundler to retreive private gems from those private repositories.

### Step 1
Register a deploy key for your repository
[Bitbucket Instructions](https://confluence.atlassian.com/display/BITBUCKET/Use+deployment+keys)
[Github Instructions](https://developer.github.com/guides/managing-deploy-keys/#deploy-keys)

### Step 2
Create a ```DEPLOY_KEY``` environment variable with the private key that you registered on your Heroku
[Heroku Instructions](https://devcenter.heroku.com/articles/config-vars#setting-up-config-vars-for-a-deployed-application)

I do

```
heroku config:set DEPLOY_KEY="`cat /path/to/key`"
```

### Step 3
Create a ```DEPLOY_KNOWN_HOSTS``` environment variable with the identification keys for the repository hosts that you're going to connect to. These are the keys found in ```~/.ssh/known_hosts```.

I backed up my ~/.ssh/known_hosts, connected to each host manually via ssh, and then did:

```
heroku config:set DEPLOY_KNOWN_HOSTS="`cat ~/.ssh/known_hosts`"
```

Afterwards I restored my old known_hosts file.

### Step 4
Set your Heroku app's default buildpack to heroku-buildpack-compose
[Instructions](https://github.com/bwhmather/heroku-buildpack-compose)

You can probably use buildpack-multi, though I haven't tried.

### Step 5
Create a .buildpacks file if you already haven't in the root directory of your app. Make sure it includes this buildpack, and any other buildpacks you need. I'm using Ruby on Rails, so I have:

NOTE: Put this buildpack first!

```sh
$ cat .buildpacks

https://bitbucket.org/taxjar/heroku-buildpack-deploy-key
https://github.com/heroku/heroku-buildpack-ruby
```

### Step 6
Commit the .buildpacks file to your repository and push to Heroku.


#### Other buildpacks
This buildpack draws from prior work at


* https://github.com/siassaj/heroku-buildpack-git-deploy-keys

* https://github.com/fs-webdev/heroku-buildpack-ssh-keys

